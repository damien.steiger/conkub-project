module.exports = {
    HOST: "mysql",
    USER: "mysql",
    PASSWORD: "1234",
    DB: "persondb",
    port: "3306",
    dialect: "mysql",
    TABLE_PERSON: "person",
    PERSON_COL_ID: "ID",
    PERSON_COL_NAME: "SURNAME",
    PERSON_COL_FIRSTNAME: "FIRSTNAME",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };