const express = require('express')
const app = express()
const dbConfig = require("./db.config.js");

const mysql = require('mysql');

const database = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
});

const port = 8080

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/hey', (req, res) => {
  res.send('Hey !!!')
})

app.get('/persons', (req, res) => {
  const sqlQuery = 'SELECT * FROM ' + dbConfig.TABLE_PERSON;

  database.query(sqlQuery, (err, result) => {
      if (err) throw err;

      res.json({ 'persons': result });
  });
});

app.get('/person/:id', (req, res) => {
  var id = req.params.id;
  const sqlQuery = `SELECT * FROM ${dbConfig.TABLE_PERSON} 
    WHERE ${dbConfig.PERSON_COL_ID}=${id}`;


  database.query(sqlQuery, (err, result) => {
      if (err) throw err;

      res.json({ 'person': result });
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});