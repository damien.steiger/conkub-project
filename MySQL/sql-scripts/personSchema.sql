CREATE DATABASE IF NOT EXISTS persondb;

USE persondb;

create table person(
   ID  integer primary key,
   SURNAME   char(20),
   FIRSTNAME char(20),
   title  char(5) references Fonction(titre)
);

insert into person values (1, 'Mayer',  'Simon' ,   'PR'); 
insert into person values (150, 'Dupont',  'Martin' ,   'PR');  
insert into person values (145,'Martin',  'Michelle' , 'CC' );
insert into person values (252, 'Laslo' ,  'Martine' ,  'CS'  );
insert into person values (151, 'Perrier' ,'Vincent' ,  'PR' );